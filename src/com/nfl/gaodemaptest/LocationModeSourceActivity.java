package com.nfl.gaodemaptest;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationClientOption.AMapLocationMode;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.AMap.OnMarkerClickListener;
import com.amap.api.maps2d.CameraUpdate;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.LocationSource;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.amap.api.navi.AMapNavi;
import com.amap.api.navi.enums.PathPlanningStrategy;
import com.amap.api.navi.model.NaviLatLng;

public class LocationModeSourceActivity extends Activity {
	private MapView mapView;
	private AMap aMap;
	private AMapLocation myLocation;
	private AMapNavi mapNavi;

	// 声明AMapLocationClient类对象
	private AMapLocationClient mLocationClient;
	private AMapLocationClientOption mLocationOption;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mapView = (MapView) findViewById(R.id.map);
		mapView.onCreate(savedInstanceState);// 此方法必须重写
		init();
		initAMapLocationClient();
	}

	private void initAMapLocationClient() {
		AMapLocationClient.setApiKey("a4e11a433782090c24a0a7e692f3ffe6");
		// 初始化定位参数
		mLocationOption = new AMapLocationClientOption();
		// 设置定位模式为高精度模式Hight_Accuracy，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
		mLocationOption.setLocationMode(AMapLocationMode.Hight_Accuracy);
		// 设置是否返回地址信息（默认返回地址信息）
		mLocationOption.setNeedAddress(true);
		// 设置是否只定位一次,默认为false
		mLocationOption.setOnceLocation(false);
		// 设置是否强制刷新WIFI，默认为强制刷新
		mLocationOption.setWifiActiveScan(true);
		// 设置是否允许模拟位置,默认为false，不允许模拟位置
		mLocationOption.setMockEnable(false);
		// 设置定位间隔,单位毫秒,默认为2000ms
		mLocationOption.setInterval(2000);
		// 初始化定位
		mLocationClient = new AMapLocationClient(getApplicationContext());
		// 设置定位回调监听
		mLocationClient.setLocationListener(aMapLocationListener);
		// 给定位客户端对象设置定位参数
		mLocationClient.setLocationOption(mLocationOption);
		// 启动定位
		mLocationClient.startLocation();
	}

	/**
	 * 初始化AMap对象
	 */
	private void init() {
		if (aMap == null) {
			aMap = mapView.getMap();
			aMap.setLocationSource(locationSource);// 设置定位监听
			aMap.getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
			aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
			// 设置定位的类型为定位模式，参见类AMap。
			// aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);
			aMap.setMapType(AMap.MAP_TYPE_NORMAL);

			addMarker();

		}
		mapNavi = AMapNavi.getInstance(this);
	}

	private void addMarker() {
		MarkerOptions marker = new MarkerOptions();
		marker.position(new LatLng(36.059622f, 120.375561f));
		marker.title("青岛市国税局");
		marker.snippet("延安三路236号22楼\n83931098");
		aMap.addMarker(marker);

	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onResume() {
		super.onResume();
		mapView.onResume();
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onPause() {
		super.onPause();
		mapView.onPause();
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mapView.onSaveInstanceState(outState);
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
		mLocationClient.stopLocation();// 停止定位
	}

	private LocationSource locationSource = new LocationSource() {

		@Override
		public void deactivate() {
			// TODO Auto-generated method stub

		}

		@Override
		public void activate(OnLocationChangedListener arg0) {
			// TODO Auto-generated method stub
			if (null != mLocationClient) {
				mLocationClient.setLocationListener(aMapLocationListener);
			}

			if (null != myLocation) {
				Location location = new Location("myLocation");
				location.setLatitude(myLocation.getLatitude());
				location.setLongitude(myLocation.getLongitude());
				arg0.onLocationChanged(location);
			}

		}

	};
	// 声明定位回调监听器
	private AMapLocationListener aMapLocationListener = new AMapLocationListener() {

		@Override
		public void onLocationChanged(AMapLocation amapLocation) {
			// TODO Auto-generated method stub
			if (amapLocation != null) {
				if (amapLocation.getErrorCode() == 0) {
					myLocation = amapLocation;
					LatLng latLng = new LatLng(amapLocation.getLatitude(),
							amapLocation.getLongitude());
					float zoomLevel = aMap.getMaxZoomLevel() > 16.0f ? 16.0f
							: aMap.getMaxZoomLevel();
					CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(latLng,
							zoomLevel);
					aMap.moveCamera(cu);
					mLocationClient
							.unRegisterLocationListener(aMapLocationListener);
//					if (caculateRoute()) {
//						if (mapNavi.startNavi(AMapNavi.GPSNaviMode)) {
//
//						} else {
//							Toast.makeText(getApplicationContext(), "导航启动失败",
//									Toast.LENGTH_SHORT).show();
//						}
//					} else {
//						Toast.makeText(getApplicationContext(), "规划路径失败",
//								Toast.LENGTH_SHORT).show();
//					}
				} else {
					// 显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
					// Log.i("NFL",
					// "location Error, ErrCode:"
					// + amapLocation.getErrorCode()
					// + ", errInfo:"
					// + amapLocation.getErrorInfo());
				}
			}
		}
	};

	private OnMarkerClickListener onMarkerClickListener = new OnMarkerClickListener() {

		@Override
		public boolean onMarkerClick(Marker marker) {
			// TODO Auto-generated method stub
			return false;
		}
	};

//	private boolean caculateRoute() {
//		int driverMode = getDriverMode();
//
//		List<NaviLatLng> start = new ArrayList<NaviLatLng>();
//		NaviLatLng startLatLng = null;
//		if (null != myLocation) {
//			startLatLng = new NaviLatLng(myLocation.getLatitude(),
//					myLocation.getLongitude());
//		} else {
//			// 临时测试
//			startLatLng = new NaviLatLng(35.957899d, 120.200159d);
//		}
//		start.add(startLatLng);
//
//		List<NaviLatLng> destination = new ArrayList<NaviLatLng>();
//		NaviLatLng destinationLatLng = null;
//		destinationLatLng = new NaviLatLng(36.059622d, 120.375561d);
//		destination.add(destinationLatLng);
//
//		return mapNavi
//				.calculateDriveRoute(start, destination, null, driverMode);
//
//	}

	private int getDriverMode() {
		return PathPlanningStrategy.DRIVING_DEFAULT;
	}
}
